﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEditor;

public class MainCameraController : MonoBehaviour {
	// Based on https://www.youtube.com/watch?v=bVo0YLLO43s
    protected Transform _XForm_Camera;
    protected Transform _XForm_Parent;

    protected Vector3 _LocalRotation;
    protected Vector3 localDeltaPos;
    protected float _CameraDistance = 10f;

    public float MouseTranslationSensitivity = 4000f;
    public float MouseRotationSensitivity = 4f;
    public float ScrollSensitivity = 0.005f;
    public float OrbitDampening = 5f;
    public float ScrollDampening = 5f;
    public float CameraNearLimit = 1.0f;
    public float CameraFarLimit = 100.0f;
	private bool CameraOrientationActive = false;
    private Vector3 startPos;

    // Use this for initialization
    void Start() {
        this._XForm_Camera = this.transform;
        this._XForm_Parent = this.transform.parent;
		this._LocalRotation.y = 20f;
		this._XForm_Camera.localPosition = new Vector3(0f, 0f, Mathf.Lerp(this._XForm_Camera.localPosition.z,
														this._CameraDistance * -1f, Time.deltaTime * ScrollDampening));
    }


    void LateUpdate() {
#if UNITY_EDITOR
        // Ignoring mouse input when not over GameView:
        if ((EditorWindow.mouseOverWindow == null) ||
            !(EditorWindow.mouseOverWindow.ToString().Contains("UnityEditor.GameView")))
            return;
#endif
        var pointer = Mouse.current;
        if (pointer == null)
            return;
        if (pointer.rightButton.wasPressedThisFrame) {
			CameraOrientationActive = true;
		}
		if (pointer.rightButton.wasReleasedThisFrame) {
			CameraOrientationActive = false;
		}

		if (CameraOrientationActive) {
            // Rotation of the Camera based on Mouse Coordinates
            Vector2 delta = pointer.delta.ReadValue();
            if (delta.x != 0 || delta.y != 0) {
                _LocalRotation.x += delta.x * MouseRotationSensitivity;
                _LocalRotation.y -= delta.y * MouseRotationSensitivity;

                // Clamp the y Rotation to horizon and not flipping over at the top
                if (_LocalRotation.y < -90f)
                    _LocalRotation.y = -90f;
                else if (_LocalRotation.y > 90f)
                    _LocalRotation.y = 90f;
            }
		}
        //Zooming Input from our Mouse Scroll Wheel
        Vector2 deltaScroll = pointer.scroll.ReadValue();
        if (deltaScroll.y != 0f) {
            float ScrollAmount = deltaScroll.y * ScrollSensitivity;
            ScrollAmount *= (this._CameraDistance * 0.3f);
            this._CameraDistance -= ScrollAmount;
            this._CameraDistance = Mathf.Clamp(this._CameraDistance, CameraNearLimit, CameraFarLimit);
        }
        //Actual Camera Rig Transformations
        Quaternion QT = Quaternion.Euler(_LocalRotation.y, _LocalRotation.x, 0);
        this._XForm_Parent.rotation = Quaternion.Lerp(this._XForm_Parent.rotation, QT, Time.deltaTime * OrbitDampening);

        if (this._XForm_Camera.localPosition.z != this._CameraDistance * -1f) {
            this._XForm_Camera.localPosition = new Vector3(0f, 0f, Mathf.Lerp(this._XForm_Camera.localPosition.z,
                                                        this._CameraDistance * -1f, Time.deltaTime * ScrollDampening));
        }
    }
}
