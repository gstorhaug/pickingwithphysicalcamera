using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using OSC.EVEN;

public class Picker : MonoBehaviour
{
    public Material highlightMaterial;
    // The object that should indicated the aim position:
    public GameObject aimSphere;
    Material originalMaterial;
    GameObject lastHighlightedObject;
    private EVENMain theEVENMain;
    // Debug functionality (interactively display some important values):
    public bool pickInteractively = true;
    public bool warpCoordinates = false;
    // Input coordinates:
    public Vector3 inputCoordinates;
    // The warped coordinates:
    public Vector3 warpedCoordinates;
    public Vector3 rayOrigin;
    public Vector3 rayDirection;
    public Vector3 aimPosition;
    public float firstHitDistance;
    public Vector3 firstHitPosition;
    private EVENCoordinateWarper coordinateWarper;

    // HighlightObject and ClearHighlight copied from
    // https://forum.unity.com/threads/raycast-fps-highlighting-object-on-the-cursor.488609/
    void HighlightObject(GameObject gameObject)
    {
        if (lastHighlightedObject != gameObject)
        {
            ClearHighlighted();
            originalMaterial = gameObject.GetComponent<MeshRenderer>().material;
            gameObject.GetComponent<MeshRenderer>().material = highlightMaterial;
            lastHighlightedObject = gameObject;
        }
    }

    void ClearHighlighted()
    {
        if (lastHighlightedObject != null)
        {
            lastHighlightedObject.GetComponent<MeshRenderer>().material = originalMaterial;
            lastHighlightedObject = null;
        }
    }

    void Start()
    {
        if (!theEVENMain)
        {
            theEVENMain = FindObjectOfType<EVENMain>();
        }
        coordinateWarper = gameObject.AddComponent<EVENCoordinateWarper>();
    }

    void Update()
    {
        float maxDistance = float.MaxValue;
        if (pickInteractively) {
            inputCoordinates = Mouse.current.position.ReadValue();
            inputCoordinates.z = 50.0f;
        } else {
            inputCoordinates = new Vector3(800.0f, 690.0f, 50.0f);
        }
        if (warpCoordinates)
            warpedCoordinates = coordinateWarper.WarpScreenCoordinates(inputCoordinates);
        else
            warpedCoordinates = inputCoordinates;
        aimPosition = Camera.main.ScreenToWorldPoint(warpedCoordinates);
        aimSphere.transform.position = aimPosition;
        Ray ray = Camera.main.ScreenPointToRay(warpedCoordinates);
        rayOrigin = ray.origin;
        rayDirection = ray.direction;
        RaycastHit[] hits = Physics.RaycastAll(ray.origin, ray.direction, maxDistance);
        RaycastHit firstHit = new RaycastHit();
        firstHit.distance = maxDistance;
        firstHitDistance = maxDistance;
        foreach (RaycastHit hit in hits) {
            if (hit.distance < firstHit.distance) {
                firstHit = hit;
                firstHitDistance = hit.distance;
                firstHitPosition = hit.point;
            }
        }
        if (firstHit.distance == maxDistance) {
            ClearHighlighted();
            firstHitPosition = Vector3.negativeInfinity;
        }
        else
            HighlightObject(firstHit.collider.gameObject);
    }
}