﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Currently not compatible with [ExecuteInEditMode]
public class StationariesController : MonoBehaviour {

	public int count = 16;
	private int oldCount = 0;
	public float radius = 5.0f;
	private float oldRadius = 0.0f;
	private GameObject[] obj = null;

	// Use this for initialization
	void Start () {
		CreateStationaryObjects();
		SetStationaryObjectsState();
	}
	
	void Update () {
		// If any of the public values has changed, then act accordingly (recreate all objects
		// or just update the existing ones)
		if (count != oldCount) {
			CreateStationaryObjects();
		} else if (radius != oldRadius) {
			SetStationaryObjectsState();
		}
	}

	void OnApplicationQuit() {
		DeleteObjects();
	}

	void DeleteObjects() {
		if (obj != null) {
			for (int i = 0; i < obj.Length; i++) {
				DestroyImmediate(obj[i]);
				obj[i] = null;
			}
			obj = null;
		}
	}

	void CreateStationaryObjects() {
		DeleteObjects();
		oldCount = count;
		Debug.Log("<color=cyan>Creating " + count + " objects</color>");
		// Create new objects
		List<GameObject> tmp = new List<GameObject>();
		for (int i = 0; i < count; i++) {
			GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			go.name = "StationaryObject " + i.ToString();
			go.transform.parent = gameObject.transform;
			go.GetComponent<MeshRenderer>().material = Resources.Load<Material>("SimpleLit");
			tmp.Add(go);
		}
		obj = tmp.ToArray();
	}

	void SetStationaryObjectsState() {
		if (obj != null) {
			for (int i = 0; i < obj.Length; i++) {
				obj[i].transform.localPosition =
					new Vector3(radius * Mathf.Sin(Mathf.PI * 2.0f * (float)i / (float)count),
								0.0f,
								radius * Mathf.Cos(Mathf.PI * 2.0f * (float)i / (float)count));
			}
		}
	}
}
